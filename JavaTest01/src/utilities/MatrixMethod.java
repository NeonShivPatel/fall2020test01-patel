//@author Shiv Patel
//ID: 1935098
package utilities;
/*ArrayIndexOutOfBoundsException*/
public class MatrixMethod {
	
	
	//Method duplicate will take as input int[][] a and int b, and then use both of these to create a new in[][]c that will duplicate the 
	//values in the original int[][]a
	public static int[][] duplicate(int[][] a, int b) {
		int c[][] = new int [a.length][b*2];
		//int k is created for the use of duplication
		int k = 0;
		for (int i=0; i < c.length; i++) {
			for (int j=0; j < b*2; j++) {
				//int[][] a is the only one utilising the int k, since we are trying to duplicate the values in a, so we need to reset
				//k back to 0 in order to duplicate
				c[i][j] = a[i][k];
				k++;
				if (k == b) {
					k = 0;
				}
				else {
				}
			}
		}//end loop
		return c;
	}
	
	//Method print will simply print out every value in an int[][] by taking as input the int[][] and an int representing 
	//the amount of values in each row
	public static void print(int[][] a, int b) {
		for (int i=0; i < a.length; i++) {
			for (int j=0; j < b; j++) {
				System.out.println("At position where [" + i + "][" + j + "], the value is: " + a[i][j]);
			}
		}//end loop
	}
	
	//Method findColumns will find the amount of columns/values for each row in the int[][] a
	public static int findColumns(int[][] a) {
		final int MAXVALUE = 1000000000;
		int columns = 0;
		//The following try/catch code is used to find out at what number does the array not have a value, and that number 
		//will represent the amount of values we have in each row
		try {
		for (int i=0; i < a.length; i++) {
			for (int j=0; j < MAXVALUE; j++) {
				System.out.println("At position where [" + i + "][" + j + "], the value is: " + a[i][j]);
				columns++;
			}
		}//end loop
		}
		catch (ArrayIndexOutOfBoundsException ai) {
			System.out.println("The value of j when exception occured was ");
		}
		System.out.println(columns);
		return columns;
	}
}
