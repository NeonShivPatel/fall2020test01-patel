//@author Shiv Patel
//ID: 1935098
package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;

class MatrixMethodTests {

	@Test
	void testDuplicate() {
		//Hardcoded values of the 2D array, as long as each row has the same amount of values, the following code will work
		int a[][] = { {1, 2, 3}, {4, 5, 6}};
		System.out.println("There are " + a.length + " rows in this 2D array");
		System.out.println();
		int b = MatrixMethod.findColumns(a);
		System.out.println("Each row has " + b + " values");
		System.out.println();
		int c[][] = MatrixMethod.duplicate(a,b);
		System.out.println();
		System.out.println("All the values in the duplicated 2d array:");
		MatrixMethod.print(c, b*2);
		System.out.println();
		System.out.println("For comparaison's sake, here's the orginal 2d array");
		MatrixMethod.print(a, b);
		MatrixMethod.print(c, b*2);
	}

}
