//@author Shiv Patel
//ID: 1935098
package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import vehicles.Car;

class CarTests {

	@Test
	void testCar() {
		System.out.println("Testing constructor Car:");
		Car honda = new Car(5);
		System.out.println("Honda's speed is " + honda.getSpeed());
		try {
			Car toyota = new Car(-5);
		}
		catch (IllegalArgumentException e){
			System.out.println("Toyota's speed needs to be 0 or higher, you can't have negative numbers");
		}
		System.out.println("");
	}
	
	@Test
	void testGetSpeed() {
		System.out.println("Testing getSpeed:");
		Car honda = new Car(5);
		System.out.println("Honda's speed is " + honda.getSpeed());
		System.out.println("");
	}
	
	@Test
	void testGetLocation() {
		System.out.println("Testing getLocation:");
		Car honda = new Car(5);
		System.out.println("Honda's location is " + honda.getLocation());
		System.out.println("");
	}
	
	@Test
	void testMoveRight() {
		System.out.println("Testing moveRight:");
		Car honda = new Car(5);
		System.out.println("Honda's location before moving right is " + honda.getLocation());
		honda.moveRight();
		System.out.println("Honda's location after moving right is " + honda.getLocation());
		System.out.println("");
	}
	
	@Test
	void testMoveLeft() {
		System.out.println("Testing moveLeft:");
		Car honda = new Car(5);
		System.out.println("Honda's location before moving left is " + honda.getLocation());
		honda.moveLeft();
		System.out.println("Honda's location after moving left is " + honda.getLocation());
		System.out.println("");
	}
	
	@Test
	void testAccelerate() {
		System.out.println("Testing accelerate:");
		Car honda = new Car(5);
		System.out.println("Honda's speed before accelarating is " + honda.getSpeed());
		honda.accelerate();
		System.out.println("Honda's speed atfer accelarating is " + honda.getSpeed());
		System.out.println("");
	}
	
	@Test
	void testDecelerate() {
		System.out.println("Testing decelerate:");
		Car honda = new Car(5);
		System.out.println("Honda's speed before decelerating is " + honda.getSpeed());
		honda.decelerate();
		System.out.println("Honda's speed after decelerating is " + honda.getSpeed());
		System.out.println("");
	}
}
